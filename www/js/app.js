// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('dTrackr', ['ionic', 'angular-md5', 'dTrackr.controllers', 'dTrackr.services'], function($httpProvider){
    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   
    var param = function(obj) {
      
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
      
    for(name in obj) {
      value = obj[name];
        
      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }
      
    return query.length ? query.substr(0, query.length - 1) : query;
  };

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];*/ 
})

.run(function($ionicPlatform, $rootScope, $state) {
    $rootScope.baseUrl = 'http://dtrackr.net/imts/api';
    $rootScope.key = 'c1847756eb8a9680bb93b50174cd23cx';
    $rootScope.iv = '52e21c04f0526a8a57f5322c3482c86x';
    
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        
        //console.log(navigator); 
    });

    // UI Router Authentication Check
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        if (toState.data.authenticate && !$rootScope.userStatus) {
            // User isn’t authenticated
            $state.transitionTo("login");
            event.preventDefault();
        }
    });

})

.config(function($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            .state('home', {
                url: "/home",
                templateUrl: "templates/home.html",
                controller: "HomeCtrl",
                cache: false,
                data: {
                    authenticate: true
                }
            })
            .state('login', {
                url: "/login",
                templateUrl: "templates/login.html",
                controller: "LoginCtrl",
                data: {
                    authenticate: false
                }
            })
            .state('absensi', {
                url: "/absensi",
                templateUrl: "templates/absensi.html",
                controller: "AbsensiCtrl",
                data: {
                    authenticate: true
                }
            })
            .state('registrasidata', {
                url: "/registrasidata",
                templateUrl: "templates/registrasidata.html",
                controller: "RegistrasiCtrl",
                data: {
                    authenticate: true
                }
            })
            .state('sinkronisasi', {
                url: "/sinkronisasi",
                templateUrl: "templates/sinkronisasi.html",
                controller: "SinkronisasiCtrl",
                data: {
                    authenticate: true
                }
            })
            // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');

    })
    // Fancy select directive
    .directive('fancySelect', function($ionicModal) {
        return {
            /* Only use as <fancy-select> tag */
            restrict: 'E',

            /* Our template */
            templateUrl: 'templates/directives/fancy-select.html',

            /* Attributes to set */
            scope: {
                'items': '=',
                /* Items list is mandatory */
                'value': '=' /* Selected value binding is mandatory */
            },

            link: function(scope, element, attrs, $filter) {

                /* Default values */
                scope.multiSelect = attrs.multiSelect === 'true' ? true : false;
                scope.allowEmpty = attrs.allowEmpty === 'false' ? false : true;

                /* Header used in ion-header-bar */
                scope.headerText = attrs.headerText || '';

                /* Text displayed on label */
                scope.text = attrs.text || '';
                scope.defaultText = attrs.text || '';

                /* Notes in the right side of the label */
                scope.noteText = attrs.noteText || '';
                scope.noteImg = attrs.noteImg || '';
                scope.noteImgClass = attrs.noteImgClass || '';

                /* Instanciate ionic modal view and set params */

                /* Some additionnal notes here : 
                 *
                 * In previous version of the directive,
                 * we were using attrs.parentSelector
                 * to open the modal box within a selector.
                 *
                 * This is handy in particular when opening
                 * the "fancy select" from the right pane of
                 * a side view.
                 *
                 * But the problem is that I had to edit ionic.bundle.js
                 * and the modal component each time ionic team
                 * make an update of the FW.
                 *
                 * Also, seems that animations do not work
                 * anymore.
                 *
                 */
                $ionicModal.fromTemplateUrl(
                    'templates/directives/fancy-select-items.html', {
                        'scope': scope,
                        'animation': 'slide-left-right-ios7'
                    }
                ).then(function(modal) {
                    scope.modal = modal;
                });

                /* Validate selection from header bar */
                scope.validate = function(event) {
                    // Construct selected values and selected text
                    if (scope.multiSelect == true) {

                        // Clear values
                        scope.value = '';
                        scope.text = '';

                        // Loop on items
                        jQuery.each(scope.items, function(index, item) {
                            if (item.checked) {
                                scope.value = scope.value + item.id + ';';
                                scope.text = scope.text + item.text + ', ';
                            }
                        });

                        // Remove trailing comma
                        scope.value = scope.value.substr(0, scope.value.length - 1);
                        scope.text = scope.text.substr(0, scope.text.length - 2);
                    }

                    // Select first value if not nullable
                    if (typeof scope.value == 'undefined' || scope.value == '' || scope.value == null) {
                        if (scope.allowEmpty == false) {
                            scope.value = scope.items[0].id;
                            scope.text = scope.items[0].text;

                            // Check for multi select
                            scope.items[0].checked = true;
                        } else {
                            scope.text = scope.defaultText;
                        }
                    }

                    // Hide modal
                    scope.hideItems();
                }

                /* Show list */
                scope.showItems = function(event) {
                    event.preventDefault();
                    scope.modal.show();
                }

                /* Hide list */
                scope.hideItems = function() {
                    scope.modal.hide();
                }

                /* Destroy modal */
                scope.$on('$destroy', function() {
                    scope.modal.remove();
                });

                /* Validate single with data */
                scope.validateSingle = function(item) {

                    // Set selected text
                    scope.text = item.text;

                    // Set selected value
                    scope.value = item.id;

                    // Hide items
                    scope.hideItems();
                }
            }
        };
    });;
