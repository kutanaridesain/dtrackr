var isAbsensiOk = false;
angular.module('dTrackr.controllers', ['ngCordova'])
    
    .controller('LoginCtrl', function($scope, $rootScope, $state, $ionicPopup, md5, dTrackrService) {
            
        $scope.data = {};
    
        $scope.init = function(){
            $scope.data = {};
        }
    
        $scope.login = function login(isValid) {
            
            //check connection
           if( navigator.onLine == false ) {
                var alertPopUp = $ionicPopup.alert({
                    title: "dTrackr",
                    template: "Please check your internet connection or try again later."
                });
               return;
           }
            
            if ( isValid ) { 
                
                /* 
                    parameters: x, username, password
                    return: 
                        -true : status, message, userid, token
                        -false: status, message
                */
                $scope.param = {x:"auth", username:$scope.data.username, password:md5.createHash($scope.data.password)};
                $scope.encrypted = CryptoJS.AES.encrypt(JSON.stringify($scope.param), $rootScope.key, $rootScope.key,{iv: $rootScope.vi,mode: CryptoJS.mode.CBC,padding: CryptoJS.pad.Pkcs7});
                
                dTrackrService.doLogin ($scope.encrypted.toString())
                    .then(
                    function( data ) {
                        if(data.status ){
                            $scope.data.username = '';
                            $scope.data.password = '';
                            $rootScope.userStatus = true;
                            $rootScope.token = data.token;
                            $rootScope.id_agent  = data.id_agent;
                            $rootScope.id=data.id;
                            $state.go('home');
                        }
                        else{
                            var alertPopUp = $ionicPopup.alert({
                                title: "dTrackr",
                                template: data.error + ' (' + data.code + ').' 
                            });
                        }
                    }
                );    
            }else{
                var alertPopUp = $ionicPopup.alert({
                    title: "dTrackr",
                    template: "The Username or password is not valid."
                });
            }
        }
    })

    .controller('HomeCtrl', function($scope, $state, $rootScope) {

        //$rootScope.isAbsensiOk = true;
//        console.log('absensi');
//        console.log($rootScope.isAbsensiOk);
//        console.log($rootScope.token);

        $scope.init = function init () {
            $rootScope.datacount = 0;
            if(localStorage.userData != null){
                if(typeof localStorage.userData != "undefined" && (localStorage.userData != '')){
                    var newData = JSON.parse(localStorage.userData);
                    $rootScope.datacount = newData ? newData.length : 0 ; 
                }
            }
            
            $rootScope.isAbsensiOk = isAbsensiOk;
            //$rootScope.isAbsensiOk = true;
            //console.log(localStorage.userData);
        }

        $scope.absensi = function absensi() {
            $state.go('absensi');
        }

        $scope.registrasiData = function registrasiData() {
            $state.go('registrasidata');
        }

        $scope.syncronize = function syncronize() {
            $state.go('sinkronisasi');
        }

        $scope.logout = function logout() {
            $rootScope.userStatus = false;
            $rootScope.isAbsensiOk = false;
            isAbsensiOk = false;
            $state.go('login');
            //return LoginService.logout();
        }
    })

    .controller('AbsensiCtrl', function($scope, $rootScope, $state, $cordovaBarcodeScanner, $ionicPopup, $ionicLoading, dTrackrService) {
        
        $scope.onSwipeRight = function ononSwipeRight() {
            $rootScope.isAbsensiOk = true;
            $state.go('home');
        }
        
        $scope.scanCode = function scanCode() {
            
//            if( navigator.onLine == false ) {
//                var alertPopUp = $ionicPopup.alert({
//                    title: "dTrackr",
//                    template: "Tidak dapat terhubung ke server, silahkan periksa koneksi internet Anda."
//                });
//               return;
//           }
            
            //var id = $rootScope.id;
            var token = $rootScope.token;
            var longitude = null;
            var latitude = null;
            var latlng  = null;
            
            $ionicLoading.show({
              template: 'Retrieve Data Location ...'
            });
            
            navigator.geolocation.getAccurateCurrentPosition(function(pos) {
                
                $scope.$apply(function() {
                    longitude = pos.coords.longitude;
                    latitude = pos.coords.latitude;
                    latlng = latitude + "," + longitude;
                    //latlng = '3.53468,98.649257';
                    $ionicLoading.hide();
                    
                    alert( latlng );
                    
                    $cordovaBarcodeScanner.scan().then(function(imageData) {
                        //alert("Barcode Format -> " + imageData.format);
                        //alert("Cancelled -> " + imageData.cancelled);
                        //alert(imageData.text);

                        //$rootScope.isAbsensiOk = false;
                        isAbsensiOk = false;
                        try{
                        
                            if (imageData.cancelled != true) { 

                                 var barcode = imageData.text;

                                /* 
                                    parameters: id, token, barcode, longitude, latitude
                                    return: 
                                        -true : status, message
                                        -false: status, message
                                */

                                $scope.param = {x:"checkin", token:token, latlng:latlng , hash: barcode };

                                $scope.encrypted = CryptoJS.AES.encrypt(JSON.stringify($scope.param), $rootScope.key, $rootScope.key,{iv: $rootScope.vi,mode: CryptoJS.mode.CBC,padding: CryptoJS.pad.Pkcs7});


                                dTrackrService.doAbsensi ($scope.encrypted.toString())
                                    .then(
                                    function( data ) {
                                        //console.log(data);

                                        if(data.status){
                                            //$rootScope.isAbsensiOk = true;
                                            //$scope.$apply(function() {
                                            //    $rootScope.isAbsensiOk = true;
                                            //});

                                            isAbsensiOk = true;

                                            var alertPopUp = $ionicPopup.alert({
                                                title: "Check-in",
                                                template: "You have successfully checked in.",
                                                buttons: [{
                                                    text: '<b>OK</b>',
                                                    type: 'button-positive',
                                                    onTap: function(e) {
                                                        $state.go('home');
                                                    }
                                                }]
                                            });
                                        }
                                        else{
                                            var alertPopUp = $ionicPopup.alert({
                                                title: "dTrackr",
                                                template: data.error + ' (' + data.code + ').'
                                            });
                                        }
                                    }
                                );

                            }
                        }catch(e){
                            alert(e.message);
                        } 
                    });
                });
            },
            function(err){
                $ionicLoading.hide();
                alert(err.code + "-" + err.message);
            }, function(){}, {desiredAccuracy:20, maxWait:60000, enableHighAccuracy: true});
        }
    })

    .controller('RegistrasiCtrl', function($scope, $rootScope, $state, $ionicPopup, $ionicModal, dTrackrService) {
        //console.log('controller registrasi');
        //var canvas = document.getElementById('signatureCanvas');
        $scope.signaturePad;
        $scope.canvas = null;
        $scope.image = null;
        $scope.data = {};
        
        $scope.init=function(){
            //var canvas = document.getElementById('signatureCanvas');
            $scope.data.x='customerregistration';
            //$scope.data.id = $rootScope.id;
            $scope.data.token = $rootScope.token;
            
        }
        
        $scope.gotoHome = function gotoHome() {
            $state.go('home');
        }
         
        $ionicModal.fromTemplateUrl('templates/sign-canvas.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });
            
        $scope.closeLogin = function() {
            
            $scope.data.image = "";
            
            if(!$scope.signaturePad.isEmpty()){
                var image = new Image();
                image.id = "signature"
                image.src = $scope.canvas.toDataURL();
                $scope.data.image = image.src;
                document.getElementById('sign-image').innerHTML = "";
                document.getElementById('sign-image').appendChild(image);
            }
            $scope.modal.hide();
        };
        
        $scope.tandaTangan = function tandaTangan() {      
            $scope.modal.show();
            $scope.canvas = document.getElementById('signatureCanvas');
            $scope.signaturePad = new SignaturePad($scope.canvas);
        }
        
        $scope.register = function(isValid) {
            // check to make sure the form is completely valid
            if (isValid) {
                
                if($scope.signaturePad == null){
                    var alertPopUp = $ionicPopup.alert({
                        title: "dTrackr",
                        template: "Please provide your signature here."
                    });
                    return;
                }
                
                if($scope.signaturePad.isEmpty()){
                    var alertPopUp = $ionicPopup.alert({
                        title: "dTrackr",
                        template: "Please provide your signature here."
                    });
                    return;
                }
                    
                //simpan data offline
                var token       = $rootScope.token;
                var id_agent    = $rootScope.id_agent;
                var data        = $scope.data;
                var nama        = data.nama;
                var nik         = data.nik;
                var alamat      = data.alamat;
                var no_telp     = data.noHandphone;
                var email       = data.email;
                var pekerjaan   = data.pekerjaan;
                var tanda_tangan  = data.image;
                var date        = new Date();
                
                var id_local = date.getFullYear()+''+date.getMonth()+date.getDate()+date.getHours()+date.getMinutes()+date.getSeconds();
                
                $scope.param = {x:"customerregistration", token:token, id_agent:id_agent, id_local:id_local, data:{ nama:nama, nik:nik, alamat:alamat, no_telp:no_telp, email:email, tanda_tangan:tanda_tangan, pekerjaan:pekerjaan}};
                
                
                var tempData = [];
                
                //console.log(localStorage.userData);
                
                if(typeof localStorage.userData != "undefined" && (localStorage.userData != '')){
                    tempData = JSON.parse(localStorage.userData);
                    tempData.push($scope.param);
                }else{
                    tempData = [$scope.param];
                }
                
                localStorage.userData = JSON.stringify(tempData);
                                
                $rootScope.datacount = tempData ? tempData.length : 0 ;
                
                $scope.signaturePad.clear();
                document.getElementById('sign-image').innerHTML = "";
                $scope.data = {};
                
                //check internet connection
                if( navigator.onLine == false ) {
                    var alertPopUp = $ionicPopup.alert({
                        title: "dTrackr",
                        template: "The internet connection is not available, the data will be stored on the mobile device. Please try to synchronize the data later."
                    });
                    
                    return;
                }
                //return;
                /* 
                    parameters: id, token, nama, nik, alamat, noHandphone, email, pekerjaan, image,
                    return: 
                        -true : status, message
                        -false: status, message
                */
                
                //$scope.param = $scope.data;
                
                $scope.encrypted = CryptoJS.AES.encrypt(JSON.stringify($scope.param), $rootScope.key, $rootScope.key,{iv: $rootScope.vi,mode: CryptoJS.mode.CBC,padding: CryptoJS.pad.Pkcs7});
                
                dTrackrService.doRegistration( $scope.encrypted.toString() )
                    .then(
                    function( data ) {
                        if(data.status){
                            //$scope.$apply(function() {
                                $scope.data.id = '';
                                $scope.data.token = '';
                                $scope.data.nama = '';
                                $scope.data.nik = '';
                                $scope.data.alamat = '';
                                $scope.data.noHandphone = '';
                                $scope.data.email = '';
                                $scope.data.pekerjaan = '';
                                $scope.data.image = '';
                                $scope.data = {};
                                document.getElementById('sign-image').innerHTML = "";
                            //});
                            
                            var newData = [];
                            
                            //console.log($rootScope.datacount);
                            
                            for(var i=0;i<$rootScope.datacount; i++){
                                //console.log(tempData[i].id_local + "==" + data.id_local);
                                if(tempData[i].id_local != data.id_local ){
                                    newData.push(tempData[i]);
                                }
                            }

                            var newDataLength = newData? newData.length:0;
                            //console.log(newDataLength);
                            //console.log($rootScope.datacount);
                            if(newDataLength > 0){                 
                                localStorage.userData = JSON.stringify(newData);
                            }else{
                                localStorage.userData = '';
                            }
                            
                            var alertPopUp = $ionicPopup.alert({
                                title: "dTrackr",
                                template: data.message 
                            });
                        }
                        else{    
                            var alertPopUp = $ionicPopup.alert({
                                title: "dTrackr",
                                template: data.error
                            });
                        }
                    }
                );
                
            }else{
                var alertPopUp = $ionicPopup.alert({
                    title: "dTrackr",
                    template: "The registration data is not valid."
                });
            }
        }
        
        /*$scope.simpan = function simpan(data) {
            var tempData = [];
            if(localStorage.userData!='null'){
                tempData = JSON.parse(localStorage.userData);
                tempData.push(data);
            }else{
                tempData = [data];
            }
            localStorage.userData = JSON.stringify(tempData);
            var newData = JSON.parse(localStorage.userData);
            $rootScope.datacount = newData ? newData.length : 0 ;
            var alertPopUp = $ionicPopup.alert({
                title: "dTrackr",
                template: "Data sukses tersimpan!"
            });
            alert(signaturePad.toString());
            signaturePad.clear();
        }*/
        

        $scope.onSwipeRight = function ononSwipeRight() {
            $state.go('home');
        }
    })
    
    
    .controller('SinkronisasiCtrl', function($scope, $state, $rootScope, $ionicLoading, dTrackrService) {
        $scope.syncronize = function synchronize() {
            //localStorage.userData = null;
            //$rootScope.datacount = null;
            //console.log(localStorage.userData);
            
            $scope.empty = $rootScope.datacount > 0? false:true;
            
            if(localStorage.userData != null){
                if(typeof localStorage.userData != "undefined" && (localStorage.userData != '')){
                    var dataLocal = JSON.parse(localStorage.userData);
                    $rootScope.datacount = dataLocal ? dataLocal.length : 0 ; 
                    if($rootScope.datacount > 0){
                        for(var i=0;i<$rootScope.datacount;i++){
                            //console.log(dataLocal[i]);
                            //console.log('<br/>');
                            $scope.show();
                            
                            $scope.encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataLocal[i]), $rootScope.key, $rootScope.key,{iv: $rootScope.vi,mode: CryptoJS.mode.CBC,padding: CryptoJS.pad.Pkcs7});
                
                            dTrackrService.doRegistration( $scope.encrypted.toString() )
                                .then(
                                function( data ) {
                                    if(data.status){
                                        
                                        var newData = [];

                                        for(var i=0;i<$rootScope.datacount; i++){
                                            //console.log(dataLocal[i].id_local + "==" + data.id_local);
                                            if(dataLocal[i].id_local != data.id_local ){
                                                newData.push(dataLocal[i]);
                                            }
                                        }

                                        var newDataLength = newData? newData.length:0;
                                        
                                        if(newDataLength > 0){                 
                                            localStorage.userData = JSON.stringify(newData);
                                        }else{
                                            localStorage.userData = '';
                                        }
                                        
                                        $rootScope.datacount = newDataLength;
                                        $scope.empty = $rootScope.datacount > 0? false:true;
                                        $scope.hide();
                                        
                                        if(newDataLength == 0){
                                            $state.go('home');
                                        }
                                    }
                                }
                            )
                        }
                    }
                }
            }
            
            //setTimeout(function(){
                $scope.hide();
            //}, 1000);
        };

        $scope.show = function show() {
            $ionicLoading.show({
                // template: 'Melakukan sinkronisasi ...',
                // content: 'Melakukan sinkronisasi ...<i class="icon ion-loading-c"></i>',
                templateUrl: 'templates/loading.html',
                // duration: 3000,
                hideOnStateChange: true,
                animation: 'fade-in',
                showBackdrop: false,
            });
        };
        $scope.hide = function hide() {
            $ionicLoading.hide();
        };


        $scope.onSwipeRight = function ononSwipeRight() {
            $state.go('home');
        };

    });
