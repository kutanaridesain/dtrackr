angular.module('dTrackr.services', [])
//    .factory('LoginService', function($q, $state, $rootScope) {
//        return {
//            isLoggedIn: function() {
//                return $rootScope.userStatus;
//            },
//
//            loginUser: function(username, password) {
//                var deferred = $q.defer();
//                var promise = deferred.promise;
//
//                if (username == 'fery' && password == 'trg') {
//                    deferred.resolve('Selamat datang fery');
//                } else {
//                    deferred.reject('Login salah');
//                }
//                promise.success = function(fn) {
//                    // $rootScope.namaUser // nama user harusnya dibawa dari server jika ingin ditampilkan
//                    $rootScope.userStatus = true;
//                    promise.then(fn);
//                    return promise;
//                }
//                promise.error = function(fn) {
//                    promise.then(null, fn);
//                    return promise;
//                }
//                return promise;
//            },
//
//            logout: function() {
//                $rootScope.userStatus = false;
//                $rootScope.isAbsensiOk = false;
//                // $rootScope.namaUser = null
//                $state.go('login');
//            }
//        }
//    })


.factory('dTrackrService', function ($rootScope, $http, $q, $ionicLoading) {
    
    return({
        doLogin:doLogin,
        doAbsensi:doAbsensi,
        doRegistration: doRegistration
    });
    
    function doLogin(data) {
        $ionicLoading.show({
          template: 'User validation ...'
        });
    
        var request = $http({
            method: "post",
            url: $rootScope.baseUrl,
            data: data,
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  
        });

        return( request.then( handleSuccess, handleError ) );
    }
    
//    function doLogout(data) {
//         $ionicLoading.show({
//          template: 'Loading...'
//        });
//        
//        var request = $http({
//            method: "post",
//            url: "http://dtrackr.net/do-logout",
//            data: data,
//            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  
//        });
//
//        return( request.then( handleSuccess, handleError ) );
//    }
    
    function doAbsensi(data) {
        $ionicLoading.show({
          template: 'Checking QR Code ...'
        });
        
        var request = $http({
            method: "post",
            url: $rootScope.baseUrl,
            data: data,
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  
        });
        
        return( request.then( handleSuccess, handleError ) );
    }
    
    function doRegistration(data) {
        $ionicLoading.show({
          template: 'Store Data ...'
        });
        
        var request = $http({
            method: "post",
            url: $rootScope.baseUrl,
            data: data,
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  
        });

        return( request.then( handleSuccess, handleError ) );
    }
    
    function handleError( response ) {
        $ionicLoading.hide();
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
        ) {
            return( $q.reject( { "status":"error", "message":"An unknown error occurred." } ) );
        }
        return( $q.reject( { "status":"error", "message":response.data.message } ) );
    }
    
    function handleSuccess( response ) {
        $ionicLoading.hide();
        return( response.data );
    }
});